import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.page.html',
  styleUrls: ['./testing.page.scss'],
})
export class TestingPage implements OnInit {
  @Input() personList: string[];

  constructor() {}

  ngOnInit() {}
}
